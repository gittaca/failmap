from django.apps import AppConfig


class MonitoryourConfig(AppConfig):
    name = 'failmap.monitoryour'
