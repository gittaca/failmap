# Generated by Django 2.0.4 on 2018-04-04 13:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hypersh', '0005_merge_20180403_1600'),
    ]

    operations = [
        migrations.AddField(
            model_name='containerconfiguration',
            name='instance_type',
            field=models.CharField(default='S1', max_length=2),
        ),
        migrations.AddField(
            model_name='containerconfiguration',
            name='volumes_from',
            field=models.CharField(default='', help_text='Comma separated list of volumes.', max_length=200),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='containerconfiguration',
            name='command',
            field=models.CharField(default='celery worker --loglevel=info --concurrency=1', max_length=200),
        ),
        migrations.AlterField(
            model_name='containerenvironment',
            name='value',
            field=models.TextField(),
        ),
    ]
