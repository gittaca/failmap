��          �      �       0     1  $   >  2   c  	   �  Q   �     �     �               4     9     ?  k  N     �  )   �  )   �  	   $  5   .  	   d     n     z     �     �     �     �                             	      
                       Contest name Incorrect secret or team. Try again! Invalid or missing suffix (like .com etc): %(url)s Team name This organization %(organization)s already exists in the database for this group. contest contests organisation submission organisation submissions team teams url submission Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-07-10 10:59+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Naam van de wedstrijd Verkeerd geheim of team. Probeer opnieuw! Ongeldig suffix (zoals .com etc): %(url)s Team-naam De organisatie %(organization)s zit al in deze groep. wedstrijd wedstrijden organisatie inzending organisatie inzendingen team teams url inzending 