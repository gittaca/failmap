from django.apps import AppConfig


class GameConfig(AppConfig):
    name = 'failmap.game'
