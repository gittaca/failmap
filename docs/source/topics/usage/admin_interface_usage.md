

## Manual scans

### Command line
The Scan command can help you:

```bash
failmap scan 'scanner name'
```

The message returned will tell you what scanners you can run manually. All scanners have the same set of options.

### Admin interface
It's possible to run manual scans, at the bottom of a selection.
Note that this is beta functionality and please don't do this too much as the "priority" scanning queue is not functioning.
You can try out a scan or two, some take a lot of time.

![admin_actions](scanners_scanning_and_ratings/admin_actions.png)
