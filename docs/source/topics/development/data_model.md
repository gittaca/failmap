This is an autogenerated page about the FailMap data model.
# organizations
![Data Model](data_model/organizations_models.png)

# scanners
![Data Model](data_model/scanners_models.png)

# map
![Data Model](data_model/map_models.png)

# game
![Data Model](data_model/game_models.png)

# app
![Data Model](data_model/app_models.png)

# hypersh
![Data Model](data_model/hypersh_models.png)

# All in one
![Data Model](data_model/failmap_models.png)

